const code = document.getElementById('code');
const test_btn = document.getElementById('test-btn');
const code_result = document.getElementById('code-result');

window.addEventListener('scroll', () => {
    Array.from(document.getElementsByClassName('parallax-effect')).forEach(e => {
        e.style.transform = 'translateY(' + window.pageYOffset * e.getAttribute('speed') + 'px)';
    });
});

/* Allow tab key in textarea */
code.addEventListener('keydown', function (e) {
    if (e.key != 'Tab') {
        return;
    }

    e.preventDefault();
    let start = this.selectionStart;
    this.value = this.value.substring(0, start) + "\t" + this.value.substring(this.selectionEnd);
    this.selectionStart = this.selectionEnd = start + 1;
});

function getOutput() {
    const fd = new FormData();
    fd.append('code', code.value);
    test_btn.src = './icons/stop.png';
    code_result.style.opacity = 0.6;

    fetch('./getOutput', {
        method: 'POST',
        body: fd,
    })
    .then(res => res.json())
    .then(res => {
        code_result.textContent = res.output;
        code_result.style.opacity = 1;
    })
    .catch(e => alert(e))
    .finally(() => test_btn.src = './icons/play.png');
}

function getOS() {
    let platform = window.navigator.platform;

    if ([ 'Macintosh', 'MacIntel', 'MacPPC', 'Mac68K' ].indexOf(platform) !== -1) {
        return 'mac';
    } else if ([ 'Win32', 'Win64', 'Windows', 'WinCE' ].indexOf(platform) !== -1) {
        return 'windows';
    }

    return 'linux';
}

(() => {
    let download_btn = document.getElementById('download-btn');
    download_btn.href += getOS();
    getOutput();
})();
