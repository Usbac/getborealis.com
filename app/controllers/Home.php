<?php

namespace Controller;

use Wolff\Core\Cache;
use Wolff\Core\Language;
use Wolff\Core\View;

class Home extends \Wolff\Core\Controller
{

    public function index()
    {
        View::render('home', [
            'lang'    => Language::get('main'),
            'page'    => 'home',
            'version' => Cache::get('current_version'),
            'codes'   => [
                View::getSource('code/feature_1.html'),
                View::getSource('code/feature_2.html'),
                View::getSource('code/feature_3.html'),
            ],
        ]);
    }


    public function getOutput($req, $res)
    {
        $tmp_name = tempnam(sys_get_temp_dir(), 'BOR_');
        $fp = fopen($tmp_name, 'w+');
        fwrite($fp, $req->body('code'));
        fclose($fp);
        chmod($tmp_name, 0775);

        $output = null;
        exec("timeout 2s " . path('system/borealis') . " -f $tmp_name", $output, $r);
        unlink($tmp_name);

        $res->writeJson([
            'output' => implode(PHP_EOL, $output),
        ]);
    }
}
