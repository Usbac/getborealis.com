<?php

namespace Controller;

use Wolff\Core\Cache;
use Wolff\Core\Language;
use Wolff\Core\View;

class Download extends \Wolff\Core\Controller
{

    public function index()
    {
        $version = Cache::get('current_version');
        View::render('download', [
            'page'     => 'download',
            'lang'     => Language::get('main'),
            'releases' => Cache::get('releases_view'),
            'version'  => $version,
            'links'    => [
                'windows' => $this->getDownloadLink($version, 'windows'),
                'linux'   => $this->getDownloadLink($version, 'linux'),
                'mac'     => $this->getDownloadLink($version, 'mac'),
            ],
        ]);
    }


    private function getDownloadLink($version, $os)
    {
        return "https://github.com/Usbac/borealis/releases/download/$version/borealis-$os";
    }
}
