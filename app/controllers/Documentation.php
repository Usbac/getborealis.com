<?php

namespace Controller;

use Wolff\Core\Container;
use Wolff\Core\Language;
use Wolff\Core\View;

class Documentation extends \Wolff\Core\Controller
{

    const MATCHES_LIMIT = 7;


    public function page($req, $res)
    {
        $key = str_replace('_', '/', $req->query('page') ?? 'getting started');
        $page = Container::get('db')->query('SELECT * FROM `page` WHERE key = ?', urldecode($key))->first();

        if (empty($page)) {
            $res->setCode(404);
            return;
        }

        View::render('documentation', [
            'lang'    => Language::get('main'),
            'docs'    => Language::get('docs'),
            'page'    => 'documentation',
            'pages'   => $this->getSidebarList(),
            'search'  => $req->query('q') ?? '',
            'key'     => str_replace('stdlib/', 'stdlib_', $page['key']),
            'content' => $page['content'],
        ]);
    }


    public function search($req)
    {
        $search = $req->query('q') ?? '';
        if (empty($search)) {
            return redirect(url('docs/'));
        }

        $rows = Container::get('db')->select('page', '`content` LIKE (\'%\' || ? || \'%\') COLLATE NOCASE', strip_tags($search));

        foreach ($rows as $key => $row) {
            $lines = explode("\n", strip_tags($row['content']));
            $lines_n = count($lines);
            $rows[$key]['matches'] = [];

            if (strlen($search) <= 2) {
                continue;
            }

            $line_key = -1;
            for ($i = 0; $i < $lines_n && count($rows[$key]['matches']) < self::MATCHES_LIMIT; $i++) {
                if (stripos($lines[$i], $search) === false) {
                    continue;
                }

                $line_key += substr_count(strtoupper($lines[$i]), strtoupper($search));
                $rows[$key]['matches'][$line_key] = strip_tags($lines[$i]);
            }

            if (count($rows[$key]['matches']) <= 0) {
                unset($rows[$key]);
                continue;
            }
        }

        usort($rows, function ($a, $b) {
            return count($a['matches']) < count($b['matches']);
        });

        View::render('documentation', [
            'lang'    => Language::get('main'),
            'docs'    => Language::get('docs'),
            'page'    => 'documentation',
            'pages'   => $this->getSidebarList(),
            'key'     => 'search',
            'search'  => $search,
            'content' => View::getRender('partials/search', [
                'lang'    => Language::get('docs'),
                'search'  => $search,
                'results' => $rows,
            ]),
        ]);
    }


    private function getSidebarList()
    {
        $pages = [];
        foreach (Container::get('db')->select('page.key', '1 ORDER BY key') as $file) {
            $paths = explode('/', $file);

            switch (count($paths)) {
                case 1: $pages[] = $paths[0]; break;
                case 2: $pages[$paths[0]][] = $paths[1]; break;
            }
        }

        return $pages;
    }
}
