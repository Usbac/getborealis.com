<?php

namespace Model;

use Wolff\Core\Container;
use Wolff\Core\View;
use Wolff\Utils\Str;

class Cache
{

    const RELEASES_NUMBER = 4;


    public function clear()
    {
        $this->clearReleases();
        $this->clearDocumentation();
    }


    private function clearReleases()
    {
        $content = '';
        $url = 'http://api.github.com/repos/Usbac/borealis/releases';

        try {
            $content = file_get_contents($url, false, config('stream'));
        } catch (\Exception $e) {
            die("Could not obtain $url content");
        }

        $releases = json_decode($content, true);

        \Wolff\Core\Cache::clear();

        \Wolff\Core\Cache::set('current_version', $releases[0]['tag_name']);

        $releases = array_map(function ($release) {
            return [
                'name' => $release['name'],
                'url'  => $release['html_url'],
                'body' => Container::get('markdown')->parse($release['body']),
                'date' => (new \Datetime($release['published_at']))->format('m/d/Y'),
            ];
        }, array_slice($releases, 0, self::RELEASES_NUMBER));

        \Wolff\Core\Cache::set('releases_view', View::getRender('partials/releases', [
            'releases' => $releases,
        ]));
    }


    private function clearDocumentation()
    {
        $zip_path = path('borealis.zip');
        $url = 'https://github.com/Usbac/borealis/archive/refs/heads/main.zip';
        $content = '';

        try {
            $content = file_get_contents($url);
        } catch (\Exception $e) {
            die("Could not obtain $url content");
        }

        file_put_contents($zip_path, $content);

        $zip = new \ZipArchive;
        if ($zip->open($zip_path) === true) {
            $zip->extractTo(path('system'));
            $zip->close();
        } else {
            die();
        }

        unlink($zip_path);

        $docs_path = path('system/borealis-main/docs/' . config('language') . '/');
        $ite = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($docs_path));
        $files = array_filter(iterator_to_array($ite), function($f) {
            return $f->isFile();
        });

        $files = array_map(function ($file) use ($docs_path) {
            return Str::before(Str::after($file, $docs_path), '.md');
        }, array_keys($files));

        Container::get('db')->delete('page');
        foreach ($files as $file) {
            Container::get('db')->insert('page', [
                'key'     => $file,
                'content' => Container::get('markdown')->parse(file_get_contents($docs_path . "$file.md")),
            ]);
        }

        $this->rmdirR(path('system/borealis-main'));
    }


    private function rmdirR($dir) {
        foreach (array_diff(scandir($dir), [ '.', '..' ]) as $file) {
            $path = "$dir/$file";
            if (is_dir($path) && !is_link($path)) {
                $this->rmdirR($path);
            } else {
                unlink($path);
            }
        }

        return rmdir($dir);
    }
}
