<?php

return [
    'title'                 => 'Borealis',
    'subtitle'              => 'The elegant and consistent programming language',
    'home'                  => 'Home',
    'download'              => 'Download',
    'download_description'  => 'Download Borealis for your current operating system.',
    'install'               => 'Install',
    'documentation'         => 'Documentation',
    'test_info'             => "Functions that require disk, system or network access are blacklisted. \nMax execution time is set to 2 seconds.\nTesting version may not be the last language version.",
    'compiling'             => 'Compile it on your own',
    'version'               => 'Version',
    'try_it'                => 'Try it!',
    'try_it_description'    => 'Run Borealis code right from your browser.',
    'repository'            => 'Repository',
    'extension'             => 'VS Code extension',
    'license'               => 'Borealis is open source software under the MIT license.',
    'license_label'         => 'License',
    'mountain_credits'      => 'Mountain photo by <a href="https://instagram.com/bklyphoto" target="_blank">Ian Beckley</a>',
    'releases'              => 'Releases',
    'install_unix'          => 'On MacOS/Linux:',
    'install_win'           => 'On Windows:',
    'code'                  => 'Code',
    'output'                => 'Output',
    'pages'                 => 'Pages',
    'search'                => 'Search...',
    'win_description'       => '<span>Windows installation process incoming...</span>',
    'unix_description'      => '<span>Run the following command where the Borealis executable is located: <pre>sudo cp ./borealis /usr/local/bin</pre></span>
        <br>
        <span>Now just run <pre>borealis --help</pre> in your terminal.</span>
        <br>
        <span>You can also run the command <pre>sudo make install</pre> to install it using Make.</span>',
    'compiling_description' => '<span>You may want to compile the software yourself, but don\'t worry, Borealis is extremely easy to compile, the only thing that you need is a C compiler with build-essentials, like GCC or CLang.</span>
        <br>
        <span>Since the Borealis codebase follows the <a href="https://wikipedia.org/wiki/C99" target="_blank">ANSI C99</a> standard, if you can compile a Hello world program on C most probably you can compile Borealis too.</span>
        <br>
        <span>You can run the <pre>make</pre> command in the Borealis folder to compile it.</span>',
    'features' => [
        [
            'title'       => 'Consistent',
            'description' => 'Borealis offers a simple syntax and a consistent standard library. Forget about things like strange results when comparing different data types, standard functions with unexpected names or operators with weird behaviour.',
        ],
        [
            'title'       => 'Easy-to-use',
            'description' => 'Borealis tries to keep things simple and minimal, from its standard library and data types to its deployment and compilation. Even its source code has been meticulously written, following the <a href="https://wikipedia.org/wiki/C99" target="_blank">ANSI C99</a> standard.',
        ],
        [
            'title'       => 'Comprehensive',
            'description' => 'The language offers multiple features useful for reducing common development headaches, like a built-in REPL debugger, statically typed variables, different operators for strings and numbers, first-class functions and more.',
        ],
    ],
    'os'                    => [
        'linux'   => 'Linux',
        'mac'     => 'MacOS ',
        'windows' => 'Windows',
    ],
    '404_title'             => 'The page you are looking for isn\'t here.',
    '404_description'       => 'Maybe you are thinking in another language, like',
    'thanks'                => 'Thanks',
    'thanks_title'          => 'Thank you ❤️',
    'special_thanks'        => 'Special thanks',
    'thanks_description'    => 'This programming language could not be possible without some amazing free open source projects, they are listed right here, so be sure to give them love.',
    'thanks_projects'       => [
        [
            'title'       => 'utf8.h',
            'url'         => 'https://github.com/sheredom/utf8.h',
            'license'     => 'Unlicense',
            'description' => 'Used by all of the language\'s strings for giving them complete unicode support.',
        ],
        [
            'title'       => 'Linenoise',
            'url'         => 'https://github.com/antirez/linenoise',
            'license'     => 'BSD-2-Clause',
            'description' => 'Used in the REPL as readline replacement .',
        ],
        [
            'title'       => 'Base64',
            'url'         => 'https://ccodearchive.net/info/base64.html',
            'license'     => 'BSD-MIT',
            'description' => 'Implementation of the base64 encoding, used by some functions of the String and File standard objects.',
        ],
        [
            'title'       => 'MD5',
            'url'         => 'https://github.com/eddelbuettel/digest/tree/master/src',
            'license'     => 'GPL',
            'description' => 'Implementation of the MD5 algorithm used by the Crypt standard object.',
        ],
        [
            'title'       => 'SHA1',
            'url'         => 'https://github.com/eddelbuettel/digest/tree/master/src',
            'license'     => 'GPL',
            'description' => 'Implementation of the SHA1 algorithm used by the Crypt standard object.',
        ],
        [
            'title'       => 'SHA256',
            'url'         => 'https://github.com/eddelbuettel/digest/tree/master/src',
            'license'     => 'GPL',
            'description' => 'Implementation of the SHA256 algorithm used by the Crypt standard object.',
        ],
    ],
];
