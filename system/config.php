<?php

return [
    'db' => [
        'dsn' => 'sqlite:' . __DIR__ . '/../db.sqlite',
    ],
    'language'       => 'en',
    'development_on' => true,
    'template_on'    => true,
    'cache_on'       => false,
    'stdlib_on'      => true,
    'maintenance_on' => false,
    'stream'         => stream_context_create([
        'http' => [
            'method' => 'GET',
            'header' => [
                'User-Agent: PHP',
            ],
        ],
    ]),
];
