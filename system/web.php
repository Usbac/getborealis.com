<?php

use Wolff\Core\Container;
use Wolff\Core\DB;
use Wolff\Core\Language;
use Wolff\Core\Route;
use Wolff\Core\View;

/* CONTAINER */

Container::singleton('db', function () {
    return new DB;
});

Container::singleton('markdown', new \cebe\markdown\GithubMarkdown());

/* ROUTES */

Route::get('/', [ Controller\Home::class, 'index' ]);

Route::post('json:getOutput', [ Controller\Home::class, 'getOutput' ]);

Route::get('download', [ Controller\Download::class, 'index' ]);

Route::get('docs/{page?}', [ Controller\Documentation::class, 'page' ]);

Route::get('docs/search', [ Controller\Documentation::class, 'search' ]);

Route::get('thanks', function() {
    View::render('thanks', [
        'lang' => Language::get('main'),
        'page' => 'thanks',
    ]);
});

Route::get('clearCache', function ($req, $res) {
    (new \Model\Cache())->clear();
    $res->setHeader('location', url(''));
});

Route::code(404, function () {
    $langs = [
        'PHP', 'Javascript', 'Java', 'Ruby', 'C', 'C++',
        'C#', 'Go', 'Haskell', 'Python', 'Perl', 'Swift',
        'Perl', 'Lua', 'Pascal', 'NASM', 'Rust', 'Objective-C',
        'R', 'Typescript', 'Io', 'Nim',
    ];

    View::render('404', [
        'lang' => Language::get('main'),
        'rand' => $langs[array_rand($langs)],
    ]);
});
