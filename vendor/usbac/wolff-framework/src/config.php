<?php

define('WOLFF_CONFIG', [
    'version'  => '4.0.1',
    'csrf_key' => '__token',
    'start'    => microtime(true),
]);
